[![License: MIT](https://img.shields.io/badge/License-MIT-yellow.svg)](https://gitlab.com/A70m0s/stephen_wozniak/-/blob/master/LICENSE)

# Stephen_Wozniak

A simple tribute to Stephen Wozniak.

# My softwares and editors

- :scroll: For the code : Visual Studio Code (from Microsoft)

![VS Code](img/vscode.PNG)

It's my favorite IDE, as a lot of people around the world ! It is versatil and easy to use, with a huge community.

- :earth_americas: For the 3d modeling : Blender 2.75 (FOSS)

![Blender](img/blender.PNG)

It's one of my favorite 3d modeling software ! It is not easy to use, but allows us to create something fastly.

- :rainbow: For the colors : [Coolors](https://coolors.co "Coolors").

![Coolors](img/coolors.PNG)

It's one of the best online color scheme generator, very simple to use (onmy press space key !).

- :camera: For images : [Unsplash](https://unsplash.com "Unsplash").

![Unsplash](img/unsplash.png)

It's a website which offer high resolution royalty free pics.